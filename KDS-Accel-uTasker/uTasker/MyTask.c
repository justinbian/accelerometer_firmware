#include "config.h"
#include <stdbool.h>

//#define START_INTERRUPT
//on which port:
#define INTERRUPT_START_PIN 			PORTB_BIT3

#define SPI_CS1_0              				PORTA_BIT8  // CS on PTA-8
#define INITIALISE_SPI_SD_INTERFACE()  		POWER_UP(4, SIM_SCGC4_SPI0); \
											_CONFIG_PERIPHERAL(B, 0, PB_0_SPI0_SCK); \
											_CONFIG_PERIPHERAL(A, 7, (PA_7_SPI0_MOSI | PORT_SRE_FAST | PORT_DSE_HIGH)); \
											_CONFIG_PERIPHERAL(A, 6, (PA_6_SPI0_MISO | PORT_PS_UP_ENABLE)); \
											_CONFIG_DRIVE_PORT_OUTPUT_VALUE(A, SPI_CS1_0, SPI_CS1_0, (PORT_SRE_FAST | PORT_DSE_HIGH));\
											SPI0_C1 = (SPI_C1_CPHA | SPI_C1_CPOL | SPI_C1_MSTR | SPI_C1_SPE); \
											SPI0_BR = (SPI_BR_SPPR_PRE_4 | SPI_BR_SPR_DIV_2); \
											(void)SPI0_S; (void)SPI0_D;

// Set maximum speed
#define SET_SPI_SD_INTERFACE_FULL_SPEED() 	SPI1_BR = (SPI_BR_SPPR_PRE_1 | SPI_BR_SPR_DIV_2)
#define WRITE_SPI_CMD(byte)    				SPI0_D = ((unsigned char) byte)
#define WAIT_TRANSMISSION_END() 				while (((unsigned char)SPI0_S & (SPI_S_SPRF)) == 0) {}
#define READ_SPI_DATA()        				(unsigned char)SPI0_D
#define SET_SD_DI_CS_HIGH()  				_SETBITS(A, SPI_CS1_0)              // force DI and CS lines high ready for the initialisation sequence
#define SET_SD_CS_LOW()      				_CLEARBITS(A, SPI_CS1_0)            // assert the CS line of the SD card to be read
#define SET_SD_CS_HIGH()     				_SETBITS(A, SPI_CS1_0)              // negate the CS line of the SD card to be read


#define SERIAL_INTERFACE
#define SET_UART_PORT() 					_CONFIG_PERIPHERAL(B, 3,  (PB_3_LPUART0_TX | UART_PULL_UPS ));\
											_CONFIG_PERIPHERAL(B, 1,  (PB_1_LPUART0_RX | UART_PULL_UPS ));
int state = 0;
QUEUE_HANDLE SerialPortID;
unsigned char keyVal;

#if defined START_INTERRUPT
#define SETUP_INTERRUPT_START_PIN()		_CONFIG_PORT_INPUT(B, INTERRUPT_START_PIN, PORT_PS_UP_ENABLE);

static void fn_my_interrupt_handler(void) {
	state = 1;
	//run the task to set up UART, SPI and PACKET READY
	uTaskerMonoTimer('Y', (DELAY_LIMIT) (0.1 * SEC), UTASKER_STOP);
}
#endif


static char twoByteRegisterRW(unsigned char wByte1, unsigned char wByte2) {
// SPI Chip Select
	WRITE_SPI_CMD(wByte1);
	WAIT_TRANSMISSION_END();
	(void)READ_SPI_DATA();
	WRITE_SPI_CMD(wByte2);
	WAIT_TRANSMISSION_END();
	return READ_SPI_DATA();

}

static void dispXYZ(receivedVal) {
	float tempVal = receivedVal;
	tempVal = tempVal / 6.505;
	fnDebugFloat(tempVal, (DISPLAY_NEGATIVE));
	fnDebugMsg("\r\n");
}

static void receivedZ(val) {
	while (val == 'z') {
		fnDebugMsg("johnny");
		fnDelayLoop(10000);
	}
}

static int temp = 1;

static void readXYZ() {
	unsigned char ctrlReg1R = 0xA0;
	unsigned char whoRegister = 0x8F;
	unsigned char xHAccelRegister = 0xA9;
	unsigned char yHAccelRegister = 0xAB;
	unsigned char zHAccelRegister = 0xAD;
	char receivedXVal;
	char receivedYVal;
	char receivedZVal;
	char inputMessage[64];
	unsigned char *msg;
	int c = 0;
	QUEUE_TRANSFER length;
	unsigned char receivedTest;

/*
	SET_SD_CS_LOW();  // SPI Chip Select
	WRITE_SPI_CMD(whoRegister);
	WAIT_TRANSMISSION_END();
	(void)READ_SPI_DATA();
	WRITE_SPI_CMD(0xFF);
	WAIT_TRANSMISSION_END();
	ucReceivedMessage = READ_SPI_DATA();
*/
/*
		// Test reading setup values for ctrlReg1
		SET_SD_CS_LOW();
		receivedTest = twoByteRegisterRW(ctrlReg1R, 0xFF);
		receivedTest = READ_SPI_DATA();
		int val = receivedTest;
		fnDebugDec(val, 0);
		SET_SD_CS_HIGH();
		fnDebugMsg("bloop\r\n");

		// Read x-axis acceleration values
		SET_SD_CS_LOW();

		receivedXVal = twoByteRegisterRW(xHAccelRegister, 0xFF);
		fnDebugMsg("x-axis: ");
		dispXYZ(receivedXVal);
		SET_SD_CS_HIGH();

		// Read y-axis acceleration values
		SET_SD_CS_LOW();

		receivedYVal =twoByteRegisterRW(yHAccelRegister, 0xFF);
		fnDebugMsg("y-axis: ");
		dispXYZ(receivedYVal);
		SET_SD_CS_HIGH();
*/
	if (fnMsgs(SerialPortID) > 0) {
		//fnDebugMsg(inputMessage);
	//msg = inputMessage;
		//fnDebugMsg(inputMessage);
		//msg = inputMessage;
		//c++;
		//fnDebugMsg(msg);
		//fnWrite(SerialPortID, &msg[c], 4);
		unsigned char ucRx[10];
		unsigned char ucLength = 5;
		ucLength = fnRead( SerialPortID, ucRx, 10);
		//fnDebugMsg(ucRx);
		if (ucRx != 0) {
			msg = (unsigned char *)ucRx;
		}

		//fnDebugMsg(msg);
		keyVal = *msg;
		//fnDebugMsg(&john);
/*
		if (msg == 'x') {

		}
	    else if (msg == 'y') {

		}*/
		if (keyVal == 'z') {
/*
			// Read z-axis acceleration values
			SET_SD_CS_LOW();

			receivedZVal = twoByteRegisterRW(zHAccelRegister, 0xFF);
			//fnDebugMsg("z-axis: ");
			dispXYZ(receivedZVal);
			SET_SD_CS_HIGH();
*/
			//state = 3;
			//uTaskerMonoTimer('Y', (DELAY_LIMIT) (0.3 * SEC), UTASKER_STOP);
		}
		else {
			//state = 2;
		}

		//fnWrite(SerialPortID, (unsigned char *)ucRx, ucLength);
	}
	if (keyVal == 'z') {
		SET_SD_CS_LOW();
		receivedZVal = twoByteRegisterRW(zHAccelRegister, 0xFF);
		dispXYZ(receivedZVal);
		SET_SD_CS_HIGH();
		uTaskerMonoTimer('Y', (DELAY_LIMIT) (0.3 * SEC), 0);
	}
	else if (keyVal == 'y') {
		SET_SD_CS_LOW();
		receivedYVal = twoByteRegisterRW(yHAccelRegister, 0xFF);
		dispXYZ(receivedYVal);
		SET_SD_CS_HIGH();
		uTaskerMonoTimer('Y', (DELAY_LIMIT) (0.3 * SEC), 0);
	}
	else if (keyVal == 'x') {
		SET_SD_CS_LOW();
		receivedXVal = twoByteRegisterRW(xHAccelRegister, 0xFF);
		dispXYZ(receivedXVal);
		SET_SD_CS_HIGH();
		uTaskerMonoTimer('Y', (DELAY_LIMIT) (0.3 * SEC), 0);
	}



}



extern void fnMyTask(TTASKTABLE* ptrTaskTable) {
	/*
	INITIALISE_SPI_SD_INTERFACE();
	INITIALIZE_MY_PIN();
	SET_SD_CS_LOW();
	WRITE_SPI_CMD(0x55);
	//WAIT_TRANSMISSON_END();
	(void)READ_SPI_DATA();
	SET_SD_CS_LOW();
	fnDelayLoop(3000);
	//SET_SD_CS_HIGH();
	uTaskerMonoTimer(TASK_MY_TASK, (DELAY_LIMIT)(1 * SEC), UTASKER_STOP);
	*/
	if (state == 0) {
		#if defined START_INTERRUPT
		// MCU Start Interrupt setup
		SETUP_INTERRUPT_START_PIN();

		INTERRUPT_SETUP interrupt_setup;
		interrupt_setup.int_type       = PORT_INTERRUPT;                    // identifier to configure port interrupt
		interrupt_setup.int_handler    = fn_my_interrupt_handler;           // handling function
		interrupt_setup.int_priority   = PRIORITY_PORT_B_INT;             	// interrupt priority level
		interrupt_setup.int_port       = PORTB;                 			// the port that the interrupt input is on
		interrupt_setup.int_port_bits  = INTERRUPT_START_PIN;           // the IRQ input connected
		interrupt_setup.int_port_sense = (IRQ_RISING_EDGE | PULLUP_ON);  	// interrupt is to be falling edge sensitive
		fnConfigureInterrupt((void *)&interrupt_setup);
		#endif

		#if !defined START_INTERRUPT
			state = 1;
			uTaskerMonoTimer('Y', (DELAY_LIMIT) (0.3 * SEC), UTASKER_STOP);
		#endif

	}
	else if (state == 1){

		SET_SD_DI_CS_HIGH();
		INITIALISE_SPI_SD_INTERFACE();
	 	SET_UART_PORT();

		TTYTABLE tInterfaceParameters; 						// table for passing information to driver
 							// UART handle to be obtained during open
		tInterfaceParameters.Channel = 0; 					// set UART channel for serial use
		tInterfaceParameters.ucSpeed = SERIAL_BAUD_19200; 	// baud rate 19200
		tInterfaceParameters.Rx_tx_sizes.RxQueueSize = 256; // input buffer size
		tInterfaceParameters.Rx_tx_sizes.TxQueueSize = 512; // output buffer size
		tInterfaceParameters.Task_to_wake = TASK_MY_TASK; 	// wake task on rx  TASK_MY_UART
		#ifdef SUPPORT_FLOW_HIGH_LOW
			tInterfaceParameters.ucFlowHighWater = 80; 		// set the flow control high in %
		   	tInterfaceParameters.ucFlowLowWater = 20; 		// set the flow control low in %
		#endif
		tInterfaceParameters.Config = (CHAR_8 + NO_PARITY + ONE_STOP + USE_XON_OFF + CHAR_MODE);
		if ((SerialPortID = fnOpen( TYPE_TTY, FOR_I_O, &tInterfaceParameters)) != 0) {
			// open the channel with defined configurations (initially inactive)
			fnDriver(SerialPortID, ( TX_ON | RX_ON), 0); // enable rx and tx
		}
		DebugHandle = SerialPortID;

		state = 2;
		unsigned char enableLow = 0xAF;
		unsigned char ctrlReg1W = 0x20;
		SET_SD_CS_LOW();
		twoByteRegisterRW(ctrlReg1W, enableLow);
		SET_SD_CS_HIGH();

	}
	else if (state == 2){
		//fnDebugMsg("hi");
		readXYZ();
	}





 	//fnDebugMsg("\r\n");
}
